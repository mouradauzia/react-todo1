import React from 'react';

const Todo = (props) => {
  return (
    <li className='todo stack-small' >
      <div className='c-cb'>
        <input id={props.id} type="checkbox" defaultChecked={props.status} onChange={ ()=>props.toggleTaskCompleted(props.id) } />
        <label className='todo-label' >{props.name}</label>
      </div>

      <div className='btn-group' >
        <button type="button" className='btn'>Edit</button>
        <button type="button" className='btn btn_danger' onClick={()=> props.deleteTask(props.id) } >Delete</button>
      </div>
    </li>

  );
};

export default Todo;